## This is a simple conversion PO to json.

## How to run application?

1. `npm install`
2. `npm run extract:po`
3. `npm run split:translations`

After running successfully above commands, we can see translate data into `locales` directory for each language.