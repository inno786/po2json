/* eslint-disable no-return-assign */
const fs = require('fs');

const setValue = (item, value, arrData, def = 'en') => {
    if (value && Array.isArray(value)) {
        arrData.map((items, idx) => (Object.keys(items)[0] === def ?
            arrData[idx][Object.keys(items)[0]] = { ...arrData[idx][Object.keys(items)[0]], [item]: item } :
            arrData[idx][Object.keys(items)[0]] = { ...arrData[idx][Object.keys(items)[0]], [item]: value[1] }));
    }
};

module.exports.splitLocales = (...params) => {
    const arrData = [];
    params.map(item => arrData.push({ [item]: {} }));
    const rawdata = fs.readFileSync('src/i18n/translations/translation.json');
    const obj = JSON.parse(rawdata);
    Object.keys(obj).map(item => setValue(item, obj[item], arrData, 'en'));
    arrData.map((item, idx) => fs.writeFile(
        `src/i18n/locales/${Object.keys(item)[0]}.json`, JSON.stringify(arrData[idx][Object.keys(item)[0]], null, 2), 'utf-8', () => { }
    ));
};
